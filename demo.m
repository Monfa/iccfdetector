
%% extract training and testing images and ground truth
cd(fileparts(which('demo.m'))); 

%Add paths
path1 = 'external';
addpath(genpath(path1));

%Extracted Caltech Data
%Put the path of the extracted data dir here.
dataDir='/media/almonfrey/Data/PedestrianDetectionData/data/Caltech/';
%New set of annotations dir.
dataDirNew='/media/almonfrey/Data/PedestrianDetectionData/Caltech_New_Data/';

rng('default')

c = randi(100,20,1);
C = unique(c,'stable');
C = [0 ; C(1:10)];

%Random seed.
seedN=C(1);

%The extracted data is the Caltech 10x -  1 frame at every 3 frames.
%The data must be extracted using the dbExtract tool of the code.3.2.1
%available at 
%http://www.vision.caltech.edu/Image_Datasets/CaltechPedestrians/code/code3.2.1.zip
skip=3;

%% set up opts for training detector (see acfTrain)
opts=acfTrainDA(); opts.modelDs=[50 20.5]; opts.modelDsPad=[64 32];
opts.pPyramid.pChns.pColor.smooth=0; opts.nWeak=[64 256 1024 4096];
opts.pBoost.pTree.maxDepth=5; opts.pBoost.discrete=0;
opts.pBoost.pTree.fracFtrs=1/16; opts.nNeg=25000; opts.nAccNeg=50000;
opts.pPyramid.pChns.pGradHist.softBin=1; opts.pJitter=struct('flip',1);
opts.posGtDir=[dataDir 'train' int2str2(skip,2) '/annotations'];
opts.posImgDir=[dataDir 'train' int2str2(skip,2) '/images'];
opts.pPyramid.pChns.shrink=2; opts.name='models/AcfCaltech+';
pLoad={'lbls',{'person'},'ilbls',{'people'},'squarify',{3,.41}};
opts.pLoad = [pLoad 'hRng',[50 inf], 'vRng',[1 1] ];

opts.modelDs=[100 41]; opts.modelDsPad=[128 64]; 
opts.pBoost.pTree.fracFtrs=1/16;
opts.pPyramid.pChns.shrink = 4;
opts.nNeg=100000;opts.nAccNeg = 200000;
opts.pJitter=struct('flip',0,'scls',[1 1; 1.1 1; 1 1.1; 1.1 1.1]);     
opts.nWeak=[64 256 1024 4096];
opts.pBoost.pTree.maxDepth = 5;
opts.pBoost.lastStage=length(opts.nWeak)-1;
opts.pBoost.numTreeFracFtrs = 512;
opts.pPyramid.nOctUp = 1; 

% Shrinkage for each stage           
opts.shrinkageValues=[0.25, 0.25, 0.25, 0.25, 0.25 0.25];
opts.pBoost.shrinkage = opts.shrinkageValues(end);
opts.nWeak=[64 512 1024 2048 4096 8192];
opts.pBoost.lastStage=length(opts.nWeak)-1;

caltechNewData=1;
if(caltechNewData)
    %New Annotations
    opts.posGtDir=[dataDirNew 'Caltech_10xtrain_aligned/anno_train10x_alignedby_RotatedFilters/'];
    %The used images are the same of the Caltech 10x extracted dataset.
    opts.posImgDir=[dataDir 'train03/images/'];
    gtDir=[dataDirNew 'Caltech_new_annotations/anno_test_1xnew/'];
else
    gtDir=[dataDir 'test/annotations'];
end

%% optionally switch to LDCF version of detector (see acfTrain)
opts.ldcf=0;
if( opts.ldcf ), opts.filters=[8 4]; opts.seed=seedN;
    opts.name=['models/ldcfCaltech']; 
    opts.pPyramid.pChns.F=-1;
    opts.pPyramid.pChns.DS=0;
    opts.aplicaPartes = 0;
    opts.pPyramid.minDs = opts.modelDs;
    opts.pPyramid.pad = ceil((opts.modelDsPad-opts.modelDs)/opts.pPyramid.pChns.shrink/2)*opts.pPyramid.pChns.shrink;
    opts.pPyramid.nApprox = 0;
end

%% optionally switch to ICA version of detector (see acfTrain)
opts.ica=1;
if( opts.ica ), opts.filters=[8 18]; opts.seed=seedN;
    opts.name=['models/icaCaltech']; 
    opts.pPyramid.pChns.F=-1;
    opts.pPyramid.pChns.DS=0;
    opts.aplicaPartes = 0;
    opts.pPyramid.minDs = opts.modelDs;
    opts.pPyramid.pad = ceil((opts.modelDsPad-opts.modelDs)/opts.pPyramid.pChns.shrink/2)*opts.pPyramid.pChns.shrink;
    opts.pPyramid.nApprox = 0;
end

%% train detector (see acfTrain)
detector = acfTrainDA( opts );

%% modify detector (see acfModify)
pModify=struct('cascThr',opts.pBoost.shrinkage*-1,'cascCal',opts.pBoost.shrinkage*.025);
detector=acfModifyDA(detector,pModify);

%% test detector and plot roc (see acfTest)
[miss,~,gt,dt]=acfTestDA('name',opts.name,'imgDir',[dataDir 'test/images'],...
  'gtDir',gtDir,'pLoad',[pLoad, 'hRng',[50 inf],...
  'vRng',[.65 1],'xRng',[5 635],'yRng',[5 475]],...
  'pModify',pModify,'reapply',0,'show',2);

