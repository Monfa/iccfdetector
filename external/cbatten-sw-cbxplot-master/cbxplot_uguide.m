x = [-2*pi:0.25:2*pi];
y = sin(x);
figure;
H = plot( x, y, x, 2.*y, x, 1+y, 1+x, y );
set(gca,'xlim',[-3 3]);


figure;
cbxplot_format_fig( 600, 300 );
H = plot( x, y, x, 2.*y, x, 1+y, 1+x, y );
set(gca,'xlim',[-3 3]);


cbxplot_format_line( H(1), 4, '-',  [0,1,0]               );
cbxplot_format_line( H(2), 2, '-.', 'red',        'oF', 8 );
cbxplot_format_line( H(3), 2, '-',  'dark-orange','sF', 8 );
cbxplot_format_line( H(4), 2, ':',  'dark-blue',  's',  8 );


cbxplot_format_fonts( 'times', 18 );


cbxplot_format_margins( [ 0, 0, 0, 0 ] );


cbxplot_export_pdf('cbxplot-tut-1');


figure;
cbxplot_format_fig( 400, 400 );
cbxplot_subplot( 2, 2 );

cbxplot_subplot(1);
H = plot( x, y, x, 2.*y, x, 1+y, 1+x, y );
set(gca,'xlim',[-3 3]);
title('Example Title');

cbxplot_subplot(2);
H = plot( x, y, x, 2.*y, x, 1+y, 1+x, y );
set(gca,'xlim',[-3 3]);
title('Example Title');

cbxplot_subplot(3);
H = plot( x, y, x, 2.*y, x, 1+y, 1+x, y );
set(gca,'xlim',[-3 3]);
title('Example Title');

cbxplot_subplot(4);
H = plot( x, y, x, 2.*y, x, 1+y, 1+x, y );
set(gca,'xlim',[-3 3]);
title('Example Title');


cbxplot_format_margins( 0, [ 0.01, 0.1 ] );


cbxplot_delete_inner_labels();
cbxplot_format_margins( 0, 0.01 );


figure;
cbxplot_format_fig( 600, 300 );
H = plot( x, y, x, 2.*y, x, 1+y, 1+x, y );
set(gca,'xlim',[-3 3]);
cbxplot_add_line( [-3,-1], [3,-1], 2, '--', 'dark-red' );
cbxplot_add_line( [-3,1],  [3,1],  2, '--', 'dark-red' );
cbxplot_add_text( [-2.9,1.1], 'max', 'times', 14, 'dark-red', 'tr' );
cbxplot_add_text( [2.9,-1.1], 'min', 'times', 14, 'dark-red', 'bl' );


