%=========================================================================
% cbxplot_rotate_labels
%=========================================================================
% USAGE:
%  cbxplot_rotate_labels( angle )
%
% Rotate labels for current axis according to given angle. Adapted from
% Andy Bliss, October 14, 2005 (C). See cbxplot-uguide.txt for more
% information.

function cbxplot_rotate_labels( angle )

  % Get current tick labels and positions then erase current labels

  labels = get( gca, 'XTickLabel' );
  xpos   = get( gca, 'XTick' );
  ypos   = get( gca, 'YTick' );

  set( gca, 'XTickLabel', [] );

  % Make new tick labels

  if angle < 180
    h = text( xpos - 0.2, ...
              repmat(ypos(1)-.05*(ypos(2)-ypos(1)),length(xpos),1), ...
              labels );
    set( h, 'HorizontalAlignment', 'right' );
    set( h, 'VerticalAlignment',   'top'   );
    set( h, 'rotation', angle );  
  else
    h = text( xpos, ...
              repmat(ypos(1)-.1*(ypos(2)-ypos(1)),length(xpos),1), ...
              labels );
    set( h, 'HorizontalAlignment', 'left' );
    set( h, 'rotation', angle );
  end

