% Removes DC component from image patches
% Data given as a matrix where each patch is one column vectors
% That is, the patches are vectorized.

function Y=removeDC(X);

% Subtract local mean gray-scale value from each patch in X to give output Y

%ALTERAÇÃO DOUGLAS
%Ao invés de computar a média por patch, computei a média por pixel, pois
%cada pixel é uma variável aleatória. Ele fazia a média do Patch e o
%resultado estava estranho, pois as propriedades do PCA não se mantinham
% M = repmat(mean(X,2),1,size(X,2));
% Y = X-M;
Y = X-ones(size(X,1),1)*mean(X);


return;
