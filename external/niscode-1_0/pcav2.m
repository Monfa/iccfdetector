function [V,E,D] = pcav2(X,patchS,w)

% do PCA on image patches
%
% INPUT variables:
% X                  matrix with image patches as columns
%
% OUTPUT variables:
% V                  whitening matrix
% E                  principal component transformation (orthogonal)
% D                  variances of the principal components


% Calculate the eigenvalues and eigenvectors of the new covariance matrix.
% covarianceMatrix = X*X'/size(X,2);

%Eu tentei utilzar o esquema do Dollár para poder gerar a matriz de variância e covariância.

chns = reshape(X,patchS(1),patchS(2),[]);
wp=w*2-1;
n=size(X,2);
% compute local auto-scorrelation using Wiener-Khinchin theorem
mus=squeeze(mean(mean(chns(:,:,:)))); sig=cell(1,n);
parfor j=1:n
T=fftshift(ifft2(abs(fft2(chns(:,:,j)-mean(mus))).^2));
sig{j}=T(floor(end/2)+1-w+(1:wp),floor(end/2)+1-w+(1:wp));
end
sig=double(mean(cat(4,sig{mus>1/50}),4));

sig=reshape(full(convmtx2(sig,w,w)),wp+w-1,wp+w-1,[]);

sig=reshape(sig(w:wp,w:wp,:),w^2,w^2); sig=(sig+sig')/2;

covarianceMatrix = X*X'/size(X,2);


[E, D] = eig(sig);

% Sort the eigenvalues  and recompute matrices
[dummy,order] = sort(diag(-D));
E = E(:,order);
d = diag(D); 
dsqrtinv = real(d.^(-0.5));
Dsqrtinv = diag(dsqrtinv(order));
D = diag(d(order));
V = Dsqrtinv*E';



