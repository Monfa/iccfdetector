%Função que gera janelas candidatas para um detector utilizando um detector
%gerador de proposals.
function bbsFinal = detectWithProposal(I, detector, detectorProp)
%
    bbs=acfDetectDA(I,detectorProp);
    
    %Só precisa ser usado com a alteração abaixo
%     pNms=detector.opts.pNms;
    
%     Is=bbApply('crop',I,bbs);
    if(detector.opts.pCNN.justProp==0)
        bbsFinal = ones(0,5);
        numDt = size(bbs,1);
        numDtFinal=0;
        for i=1:numDt
            %por enquanto esse camarada só vai retornar uma detecção.
            Is = amostraImagens(I,bbs(i,:),detector.opts.pCNN.contextPad,bbs(i,3),bbs(i,4));
            bbsAux=acfDetectDA(Is{1},detector);
%             if(~isempty(bbsAux))
%                 numDtFinal=numDtFinal+1;
%                 bbsFinal(numDtFinal,1) = bbs(i,1);
%                 bbsFinal(numDtFinal,2) = bbs(i,2);
%                 bbsFinal(numDtFinal,3) = bbs(i,3);
%                 bbsFinal(numDtFinal,4) = bbs(i,4);
%                 bbsFinal(numDtFinal,5) = bbsAux(5);
%             end

            %%%ALTERAÇAO DOUGLAS-------------------------------------------
            vetAux = bbsAux(:,5);
            [~,index]=max(vetAux);
            
            %Irá selecionar o bounding box de maior score como bounding box
            %final. Isso ocorre pois em cima do BB do proposal podem ser
            %encontrados diferentes BBs...
            if(~isempty(bbsAux))
                numDtFinal=numDtFinal+1;
                bbsFinal(numDtFinal,1) = bbsAux(index,1)+bbs(i,1);
                bbsFinal(numDtFinal,2) = bbsAux(index,2)+bbs(i,2);
                bbsFinal(numDtFinal,3) = bbsAux(index,3);
                bbsFinal(numDtFinal,4) = bbsAux(index,4);
                bbsFinal(numDtFinal,5) = bbsAux(index,5)+bbs(i,5);
            end
            %--------------------------------------------------------------
        end
    else
        bbsFinal = bbs;
    end
    %Só pode ser usado com a alteração acima
%     bbsFinal=bbNms(bbsFinal,pNms);
end