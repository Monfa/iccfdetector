%Warp bb+context to the specified size
function Is = amostraImagens(I,bbsAux,contextPad,croppedDim1,croppedDim2)

        bbsSz = [bbsAux(3), bbsAux(4)];
        bbsPad = bbsSz+contextPad;
        r = bbsPad./bbsSz; bbsAux=bbApply('resize',bbsAux,r(1),r(2));
        Is=bbApply('crop',I,bbsAux);
        Is{1} = imresize(Is{1},[croppedDim2 croppedDim1]);        
end