function bbs = acfDetect( I, detector, fileName )
% Run aggregate channel features object detector on given image(s).
%
% The input 'I' can either be a single image (or filename) or a cell array
% of images (or filenames). In the first case, the return is a set of bbs
% where each row has the format [x y w h score] and score is the confidence
% of detection. If the input is a cell array, the output is a cell array
% where each element is a set of bbs in the form above (in this case a
% parfor loop is used to speed execution). If 'fileName' is specified, the
% bbs are saved to a comma separated text file and the output is set to
% bbs=1. If saving detections for multiple images the output is stored in
% the format [imgId x y w h score] and imgId is a one-indexed image id.
%
% A cell of detectors trained with the same channels can be specified,
% detected bbs from each detector are concatenated. If using multiple
% detectors and opts.pNms.separate=1 then each bb has a sixth element
% bbType=j, where j is the j-th detector, see bbNms.m for details.
%
% USAGE
%  bbs = acfDetect( I, detector, [fileName] )
%
% INPUTS
%  I          - input image(s) of filename(s) of input image(s)
%  detector   - detector(s) trained via acfTrain
%  fileName   - [] target filename (if specified return is 1)
%
% OUTPUTS
%  bbs        - [nx5] array of bounding boxes or cell array of bbs
%
% EXAMPLE
%
% See also acfTrain, acfModify, bbGt>loadAll, bbNms
%
% Piotr's Computer Vision Matlab Toolbox      Version 3.40
% Copyright 2014 Piotr Dollar.  [pdollar-at-gmail.com]
% Licensed under the Simplified BSD License [see external/bsd.txt]

% run detector on every image
if(nargin<3), fileName=''; end; multiple=iscell(I);
if(~isempty(fileName) && exist(fileName,'file')), bbs=1; return; end
if(~multiple), bbs=acfDetectImg(I,detector); else
  n=length(I); bbs=cell(n,1);
  
%ALTERAÇÃO DOUGLAS
%Uma vez tirei esse parfor por for para poder utilizar o CUDA
  parfor i=1:n
%   for i=1:n
      
      
        bbs{i}=acfDetectImg(I{i},detector); 
  
  
  end
end

% write results to disk if fileName specified
if(isempty(fileName)), return; end
d=fileparts(fileName); if(~isempty(d)&&~exist(d,'dir')), mkdir(d); end
if( multiple ) % add image index to each bb and flatten result
  for i=1:n, bbs{i}=[ones(size(bbs{i},1),1)*i bbs{i}]; end
  bbs=cell2mat(bbs);
end
dlmwrite(fileName,bbs); bbs=1;

end

function bbs = acfDetectImg( I, detector )
% Run trained sliding-window object detector on given image.
Ds=detector; if(~iscell(Ds)), Ds={Ds}; end; nDs=length(Ds);
opts=Ds{1}.opts; pPyramid=opts.pPyramid; pNms=opts.pNms;
imreadf=opts.imreadf; imreadp=opts.imreadp;
shrink=pPyramid.pChns.shrink; pad=pPyramid.pad;
separate=nDs>1 && isfield(pNms,'separate') && pNms.separate;
% read image and compute features (including optionally applying filters)
if(all(ischar(I))), I=feval(imreadf,I,imreadp{:}); end

% %ALTERAÇÃO DOUGLAS
% %Normalização da variância
% hsi = rgb2hsi(I);
% p=hsi(:,:,3);
% p=p(:);
% p1=removeDC(p);
% patchnorms=sum(p1.^2);
% p = p./(patchnorms+detector.opts.epsilon).^(1/2);
% hsi(:,:,3)=reshape(p,size(I,1),size(I,2));
% I = hsi2rgb(hsi);
  

P=chnsPyramid(I,pPyramid); bbs=cell(P.nScales,nDs);

%COMENTÁRIO DOUGLAS
%Mais uma prova de que se algum filtro é aplicado ele faz um downsampling
%por 2... Nesse caso, se o shrink é 2, ele passa a ser 4, pois temos um
%downsampling por 0.5 logo abaixo.. Além disso shrink=shrink*2; é feito
%abaixo corroborando com essa análise.

if(isfield(opts,'filters') && ~isempty(opts.filters)), shrink=shrink*2;
  for i=1:P.nScales 
      
      
%     %ALTERAÇÃO DOUGLAS
%     %Remove a média e normaliza a variância de C
%     if(detector.opts.ica)
%         C1= P.data{i};
%         for j=1:size( C1 ,3)
%                 C2 = C1(:,:,j);
%                 C2=C2(:);
%                 C3=C2;
%                 C2 = removeDC(C2);
%                 %   Normaliza a variância antes de aplicar o PCA.
%                 patchnorms=sum(C2.^2);
%                 epsilon=detector.opts.epsilon(j);
%                 C3=C3./(ones(size(C3,1),1)*patchnorms+epsilon).^(1/2);
%                 C1(:,:,j) = reshape(C3,size( C1 ,1),size( C1 ,2));
%                 %----------------------------------------------------------
%         end
%         P.data{i}=C1;
%     end
            
  fs=opts.filters; 
  
    %ALTERAÇÃO DOUGLAS
    %se aplicaPartes=1, ele vai aplicar 2 tipos de filtros aos canais de
    %características. Os filtros especificados por F1 aparecem a frente,
    %enquanto os filtros especificados por F2 aparecem atrás.
    if(~opts.aplicaPartes)
  
        %MÉTODO 1
        %Procedimento de filtragem.
          C=repmat(P.data{i},[1 1 size(fs,4)]);
        
        %ALTERAÇÃO DOUGLAS
        %Utilização de gpuArray para tentar deixar o treinamento e a detecção mais
        %rápidos
        %     C=gpuArray(C);
        
            for j=1:size(C,3), C(:,:,j)=conv2(C(:,:,j),fs(:,:,j),'same'); end
            
        %     C=gather(C);

    else

        %MÉTODO 2-----------------------------------------------
        %ALTERAÇÃO DOUGLAS
        %Modificação procedimento de filtragem.

        C=P.data{i};

        nF1 = opts.nF1;
        nChnsF1 = opts.nChnsF1;
        C2 =[];
        for ci = 1:length(nF1)
            for cj = 1:length(nChnsF1)
                CAux=conv2(C(:,:,nChnsF1(cj)),fs(:,:,nChnsF1(cj),nF1(ci)),'same');
                C2 = cat(3,C2,CAux);
            end
        end

        nF2 = opts.nF2;
        nChnsF2 = opts.nChnsF2;

        for ci = 1:length(nF2)
            for cj = 1:length(nChnsF2)
                CAux=conv2(C(:,:,nChnsF2(cj)),fs(:,:,nChnsF2(cj),nF2(ci)),'same');
                C2 = cat(3,C2,CAux);
            end
        end

        C=C2;

    end

%--------------------------------------------------

    P.data{i}=imResample(C,.5);
    
  end
end

%COMENTÁRIO DOUGLAS
%Eles fazem o stride dentro da próxima função, que é escrita em C++ e está
%com o código disponível.

% apply sliding window classifiers
for i=1:P.nScales
  for j=1:nDs, opts=Ds{j}.opts;
    modelDsPad=opts.modelDsPad; modelDs=opts.modelDs;
    bb = acfDetect1(P.data{i},Ds{j}.clf,shrink,...
      modelDsPad(1),modelDsPad(2),opts.stride,opts.cascThr);
    shift=(modelDsPad-modelDs)/2-pad;
    bb(:,1)=(bb(:,1)+shift(2))/P.scaleshw(i,2);
    bb(:,2)=(bb(:,2)+shift(1))/P.scaleshw(i,1);
    bb(:,3)=modelDs(2)/P.scales(i);
    bb(:,4)=modelDs(1)/P.scales(i);
    if(separate), bb(:,6)=j; end; bbs{i,j}=bb;
  end
end; bbs=cat(1,bbs{:});
%COMENTÁRIO DOUGLAS
%Coloquei esse print aqui para ter uma idéia do número de janelas que
%faltavam para serem amostradas.
%fprintf('\n número de janelas amostradas igual a: %d \n',length(bbs));
if(~isempty(pNms)), bbs=bbNms(bbs,pNms); end
end
