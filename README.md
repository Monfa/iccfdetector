# README #

![](logo.png)

# **Instructions**

This code has the following dependencies:

>***Piotr's Computer Vision Matlab Toolbox:**

 >>Version: 3.50;

 >>[Link](https://github.com/pdollar/toolbox);

 >>Used resources: Functions related to pedestrian detection are used;

 >>Modifications to the original code were made, so, this library is already included in the external/ folder. However, if it is used, please consider citing the original source;

 >>Some flags related to Deep Learning or Fast Guided Filter are not used in the paper, so, can not be used without additional setup.

>***Matlab evaluation/labeling:**

 >>Version: 3.2.1 (code3.2.1) tested;

 >>[Link](http://www.vision.caltech.edu/Image_Datasets/CaltechPedestrians/);

 >>Used resources: Used by the Piotr's Computer Vision Matlab Toolbox;

 >>**Note:** This library is already included in the external/ folder. However, if it is used, please consider citing the original source.
 
>***Natural Image Statistics (NIS) Book Code:**
 
 >>Version: 1_0 (niscode-1_0) tested;
 
 >>[Link](http://www.naturalimagestatistics.net/);
 
 >>Used resources: This library is used to compute ICA;
 
 >>Important information: The name of the pca function was changed to pca_nis(). Piotr's toolbox also uses the same name to compute pca. These functions have minor differences. It is also important to note that the ica() function had the lines related to the information output commented to not make the log readable;

 >>**Note:** This library is already included in the external/ folder. However, if it is used, please consider citing the original source;
  
>***Christopher Batten CbxPlot:**

 >>Version: N/A;

 >>Link: N/A;

 >>Used resources: Used to generate some "pdf" files;

 >>**Note:** This library is already included in the external/ folder. However, if it is used, please consider citing the original source.
 
>***Steps for first use**

>>A)**Download data**:

>>>Caltech pedestrian dataset can be downloaded at: 

>>>[Link](http://www.vision.caltech.edu/Image_Datasets/CaltechPedestrians/datasets/USA/);

>>>The training data (set00-set05) consists of six training sets (~1GB each), 
each with 6-13 one-minute long seq files, along with all annotation information (see the paper for details). 
The testing data (set06-set10) consists of five sets, again ~1GB each;

>>>Besides the original Caltech data, there is also the Caltech new set of annotations and the Caltech 10x aligned data, both used in the code:

>>>>New set of Annotations: 

>>>>[Link](http://datasets.d2.mpi-inf.mpg.de/caltech_new_annos/Caltech_new_annotations.zip);

>>>>Caltech 10x Aligned data: 

>>>>[Link](http://datasets.d2.mpi-inf.mpg.de/caltech_new_annos/Caltech_10xtrain_aligned.zip);

>>>If you use these Caltech New Data, please, consider citing the original work;

>>>Instruction to cite: 

>>>[Link](https://www.mpi-inf.mpg.de/departments/computer-vision-and-multimodal-computing/research/people-detection-pose-estimation-and-tracking/how-far-are-we-from-solving-pedestrian-detection/).

>>B)**Extract data**:

>>>Please download the evaluation and labeling code at: [Link](http://www.vision.caltech.edu/Image_Datasets/CaltechPedestrians/code/code3.2.1.zip)
and run 'dbExtract' to extract downloaded data to image frames and annotation text files. Please set skip=3 to obtain Caltech 10x data;

>>>Our code is based on Piotr Dollar's matlab toolbox:
>>>[Link](https://pdollar.github.io/toolbox/);
>>>Please make sure to get familiar with it before you start using our code.

>>C)**Setup paths**:

>>>Path to external/ already configured in the script demo.m.

>>>Setup the remaining paths (dataset) in the script demo.m and run the code;

>>>Note that there is a trained model inside the models/ folder. This is the model for the best result of our paper; 

>>>To retrain the detector, please delete the referred trained model;

>>>Important: As mentioned in the Piotr's toolbox: The results will differ depending on operating system and random seed (see opts.seed).


